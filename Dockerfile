FROM gwerlas/ansible

RUN apk add --no-cache ansible-lint black py3-jsonschema yamllint
