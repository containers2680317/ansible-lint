Ansible-lint
============

[![pipeline status](https://gitlab.com/yoanncolin/container-images/ansible-lint/badges/main/pipeline.svg)](https://gitlab.com/yoanncolin/container-images/ansible-lint/-/commits/main)

Official GitHub : [ansible/ansible-lint](https://github.com/ansible/ansible-lint)

Tags
----

* [`6.14.4`][latest], [`6.14`][latest], [`6`][latest], [`latest`][latest]
* [`6.14.3`][latest]
* [`6.9.1`][6.9.1], [`6.9`][6.9.1]
* [`6.8.7`][6.8.7], [`6.8`][6.8.7]
* [`6.2.1`][6.2.1], [`6.2`][6.2.1]

[latest]: https://gitlab.com/yoanncolin/container-images/ansible-lint/-/blob/main/Dockerfile
[6.14.3]: https://gitlab.com/yoanncolin/container-images/ansible-lint/-/blob/6.14.3/Dockerfile
[6.9.1]: https://gitlab.com/yoanncolin/container-images/ansible-lint/-/blob/6.9.1/Dockerfile
[6.8.7]: https://gitlab.com/yoanncolin/container-images/ansible-lint/-/blob/6.8.7/Dockerfile
[6.2.1]: https://gitlab.com/yoanncolin/container-images/ansible-lint/-/blob/6.2.1/Dockerfile

Usage
-----

Basic usage :

```sh
docker run -t --rm -v $PWD:/work -w /work gwerlas/ansible-lint ansible-lint
```

If you have installed additional collections or roles :

```sh
docker run -t --rm \
  -v $HOME/.ansible:/root/.ansible -v $PWD:/work:z -w /work \
  gwerlas/ansible-lint ansible-lint
```

Gitlab-CI usage :

```yaml
ansible-lint:
  image: gwerlas/ansible-lint
  script: ansible-lint
```

License
-------

[BSD 3-Clause License](LICENSE).
